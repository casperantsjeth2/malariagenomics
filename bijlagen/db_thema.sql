-- verwijder de tabellen voordat je ze aanmaakt, anders krijg je een error
-- verwijder ook eerst koppeltabellen en foreign key dependencies op volgorde
-- om zo errors te voorkomen
drop table if exists Coupling_table_probes_microarrays;
drop table if exists Microarrays;
drop table if exists Probes;
drop table if exists Genes;
drop table if exists Chromosomes;
drop table if exists Organisms;
-- verwijder de procedures als deze al bestaan
drop PROCEDURE if EXISTS sp_create_matrix;
drop PROCEDURE if EXISTS sp_get_matrices_by_quality;
drop PROCEDURE if EXISTS sp_get_matrix_gene_coverages;
drop PROCEDURE if EXISTS sp_get_probes_by_tm;
drop PROCEDURE if EXISTS sp_get_avg_tm_probes;
drop PROCEDURE if EXISTS sp_get_probes_per_gene;

-- maak een tabel voor organismen
create table Organisms
(   id              INT NOT NULL UNIQUE AUTO_INCREMENT,
    genus_name      VARCHAR(120) NULL,
    species_name    VARCHAR(120) NULL,
    primary key(id)
);

-- maak een tabel voor chromosomen
create table Chromosomes
(   chromosome_name VARCHAR(60) NOT NULL,
    organism_id     INT NOT NULL,
    primary key(chromosome_name),
    foreign key (organism_id)
        references Organisms(id)
);

-- maak een tabel voor genen
create table Genes
(   db_xref         VARCHAR(120) NOT NULL,
    sequence        TEXT NOT NULL,
    chromosome      VARCHAR(60) NOT NULL,
    locus_tag       VARCHAR(12) NOT NULL,

    primary key(db_xref),
    foreign key(chromosome)
        references Chromosomes(chromosome_name)
);

-- maak een tabel voor probes
create table Probes
(   id              INT NOT NULL UNIQUE AUTO_INCREMENT,
    sequence        VARCHAR(25) NOT NULL,
    db_xref         VARCHAR(120) NOT NULL,
    gc_percentage   FLOAT NULL,
    Tm              FLOAT NULL,

    primary key(id),
    foreign key(db_xref)
        references Genes(db_xref)
);

-- maak een tabel voor Microarrays
create table Microarrays
(   id              INT NOT NULL UNIQUE AUTO_INCREMENT,
    experiment      TEXT NULL,
    Tm              FLOAT NULL,
    primary key(id)
);

-- maak een koppeltabel voor Microarrays en Probes
create table Coupling_table_probes_microarrays
(   probe_id        INT NOT NULL,
    microarray_id   INT NOT NULL,
    gene_id         VARCHAR(120) NOT NULL,
    primary key(probe_id, microarray_id, gene_id),
    foreign key(probe_id)
        references Probes(id),
    foreign key(microarray_id)
        references Microarrays(id),
    foreign key(gene_id)
        references Genes(db_xref)
);

-- declare all indexes

CREATE INDEX scientific_name ON Organisms (species_name, genus_name);
CREATE INDEX tm_gc ON Probes (Tm, gc_percentage);
CREATE INDEX Tm ON Probes (Tm);
CREATE INDEX gc_percentage ON Probes (gc_percentage);

-- load all the data into the database

load data local infile "organism_data.csv"
into table Organisms
fields terminated by ';';

load data local infile "chromosome_data.csv"
into table Chromosomes
fields terminated by ';';

load data local infile "gene_data.csv"
into table Genes
fields terminated by ';';

load data local infile "blast_parse_db_output.csv"
into table Probes
fields terminated by ';';


-- declare stored procedure for grabbing probes certain range of temperatures
DELIMITER //
CREATE PROCEDURE `sp_get_probes_by_tm`(IN min_tm FLOAT, IN max_tm FLOAT)
BEGIN
	select * from Probes where Tm>=min_tm and Tm<=max_tm;
END;
//
DELIMITER ;

-- declare stored procedure for grabbing matrices by quality
DELIMITER //
CREATE PROCEDURE `sp_get_matrices_by_quality`()
BEGIN
select micro_array, no_genes_with_probes, no_probes_per_gene from
(select subquery.micro_array, count(subquery.micro_array) as no_genes_with_probes
from
(select m.id as micro_array
from Coupling_table_probes_microarrays c
join Microarrays m on m.id=c.microarray_id
group by m.id, gene_id)
as subquery
group by subquery.micro_array) as subq1

join

(select *, count(gene_id) as no_probes_per_gene
from
(select m.id, gene_id
from Coupling_table_probes_microarrays c
join Microarrays m on m.id=c.microarray_id
group by m.id, gene_id, probe_id)
as subquery group by id, gene_id) subq2
on subq1.micro_array=subq2.id
order by no_genes_with_probes asc, no_probes_per_gene desc;
END;
//
DELIMITER ;

-- declare stored procedure for making a new matrix
DELIMITER //
CREATE PROCEDURE `sp_create_matrix`(IN matrix_id INT, IN melting_t FLOAT, IN max_difference FLOAT)
BEGIN
  DECLARE min_tm FLOAT;
  DECLARE max_tm FLOAT;
  DECLARE martix_idtje INT;
  INSERT INTO Microarrays VALUES (matrix_id, NULL, melting_t);
  SET min_tm = melting_t - max_difference;
  SET max_tm = melting_t + max_difference;
  SET @matrix_idtje = matrix_id;
  INSERT INTO Coupling_table_probes_microarrays (probe_id, microarray_id, gene_id)
  select id, @matrix_idtje, db_xref from Probes where Tm>=min_tm and Tm<=max_tm;
END;
//
DELIMITER ;

-- declare stored procedure for getting average tm of all probes
DELIMITER //
CREATE PROCEDURE `sp_get_avg_tm_probes`()
BEGIN
	select avg(Tm) from Probes;
END;
//
DELIMITER ;

-- declare stored procedure for getting probes by gene in a microarray
DELIMITER //
CREATE PROCEDURE `sp_get_probes_per_gene`(IN matrix_id INT, IN db_xref varchar(120))
BEGIN
  select p.id, p.db_xref, p.sequence, p.gc_percentage, p.Tm from Probes p
  join Coupling_table_probes_microarrays c on p.id = c.probe_id
  where c.microarray_id=matrix_id AND c.gene_id=db_xref;
END;
//
DELIMITER ;

-- declare stored procedure for show gene coverage per matrix (genes with probes/total amout of probes*100s)
DELIMITER //
CREATE PROCEDURE `sp_get_matrix_gene_coverages`()
BEGIN
  select subquery.micro_array, 
  (select count(probe_id) from Coupling_table_probes_microarrays c where subquery.micro_array=c.microarray_id) as amount_probes, 
  count(subquery.micro_array)/(select count(*) from Genes)*100 as gene_coverage 
  from (select m.id as micro_array 
  from Coupling_table_probes_microarrays c 
  join Microarrays m on m.id=c.microarray_id 
  group by m.id, gene_id) as subquery 
  group by subquery.micro_array;
END;
//
DELIMITER ;
